package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
