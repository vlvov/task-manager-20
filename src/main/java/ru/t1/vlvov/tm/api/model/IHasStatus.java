package ru.t1.vlvov.tm.api.model;

import ru.t1.vlvov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
