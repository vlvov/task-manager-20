package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    List<M> findAll();

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    boolean existsById(String id);

    List<M> findAll(Comparator comparator);

}
