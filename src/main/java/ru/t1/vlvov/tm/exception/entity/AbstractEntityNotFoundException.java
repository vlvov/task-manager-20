package ru.t1.vlvov.tm.exception.entity;

import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
        super();
    }

    public AbstractEntityNotFoundException(final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(final Throwable cause) {
        super(cause);
    }

    protected AbstractEntityNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
