package ru.t1.vlvov.tm.command.task;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Start task by Id.";

    private final String NAME = "task-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

}