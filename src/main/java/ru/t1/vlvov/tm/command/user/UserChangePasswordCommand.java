package ru.t1.vlvov.tm.command.user;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private final String NAME = "user-change-password";

    private final String DESCRIPTION = "User change password.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String id = getAuthService().getUserId();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(id, password);
    }

}
