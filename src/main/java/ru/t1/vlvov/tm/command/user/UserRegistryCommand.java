package ru.t1.vlvov.tm.command.user;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final String NAME = "user-registry";

    private final String DESCRIPTION = "User registry.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

}
