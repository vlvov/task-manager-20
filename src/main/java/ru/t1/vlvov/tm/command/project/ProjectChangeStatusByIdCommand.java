package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Change project status by Id.";

    private final String NAME = "project-change-status-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getAuthService().getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }

}
