package ru.t1.vlvov.tm.command.project;

import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.IProjectService;
import ru.t1.vlvov.tm.api.service.IProjectTaskService;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("USER ID: " + project.getUserId());
    }

}
