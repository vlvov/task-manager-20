package ru.t1.vlvov.tm.command.user;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.util.TerminalUtil;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "logout";

    private final String DESCRIPTION = "User logout.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}
